<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css">
    <title>Filtro Test</title>

    <!--Para Mostrar/Ocultar los div de busqueda -->
    <style type="text/css">
      .contrato{
          display:none;
      }
      .fecha{
          display:none;
      }
      .fecharange{
          display:none;
      }
    </style>

</head>
<body>

    <?php
        //Archivo donde se formatea el JSON
        require('json.php');
    ?>

    <!-- Select para seleccionar el tipo de busqueda -->
    <form class="select" class="select">
        <select name="search" id="select">
            <option disabled selected>Seleccione el tipo de busqueda</option>
            <option value="1">Numero de Contrato</option>
            <option value="2">Fecha del Contrato</option>
            <option value="3">Rango de Fechas</option>
        </select>
    </form>

    <!-- Busqueda por No. de Contrato -->
    <form action="busqueda-result.php" method="post" class="contrato">
        <input type="text" class="form-control" value="" name="contrato" placeholder="Numero de Contrato">
        <span>
            <input type="submit" value="Buscar">
        </span>
    </form>

    <!-- Busqueda por Fecha -->
    <form action="busqueda-result.php" method="post" class="fecha">
        <input type="text" class="form-control" value="" name="datarange">
        <span>
            <input type="submit" value="Buscar">
        </span>
    </form>

    <!-- Busqueda por Rango de Fechas -->
    <form action="busqueda-result.php" method="post" class="fecharange">
        <input type="text" class="form-control" value="" name="range">
        <span>
            <input type="submit" value="Buscar">
        </span>
    </form>


    <table border=1>
        <tr>
            <th>Numero Contrato</th>
            <th>Adjunto</th>
            <th>Objeto</th>
            <th>fecha</th>
            <th>tipo</th>
        </tr>

        <!-- Presenta el JSON en forma de talba -->
        <?php
            foreach($json as $clave => $indice){
                echo "<tr>";
                foreach($indice as $atributo => $valor){
                    if($atributo == 'adjunto'){
                        echo "<th><a href= contratos-detalle/".$valor.">Adjunto </th>";
                    }else {
                        echo "<th>" .$valor. "</th>";
                    }
                }
                echo "</tr>";
            }
        ?>

    </table>

</body>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script>
//Config datapicker to datarange
$('input[name="datarange"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    "locale": {
        "daysOfWeek": [
            "Lu",
            "Ma",
            "Mi",
            "Ju",
            "Vi",
            "Sa",
            "Do"
        ],
        "monthNames": [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ],
    }

});
</script>

<script>
//Config datapicker to range
$('input[name="range"]').daterangepicker({
    "locale": {
        "daysOfWeek": [
            "Lu",
            "Ma",
            "Mi",
            "Ju",
            "Vi",
            "Sa",
            "Do"
        ],
        "monthNames": [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ],
    }

});
</script>



<script>
//Configuracion del Select para mostrar/ocultar
$('#select').change(function () {
    if($('#select').val() == 1){
        $(".contrato").css("display", "block");
        $(".fecha").css("display", "none");
        $(".fecharange").css("display", "none");
    };
    if($('#select').val() == 2){
        $(".contrato").css("display", "none");
        $(".fecha").css("display", "block");
        $(".fecharange").css("display", "none");
    };
    if($('#select').val() == 3){
        $(".contrato").css("display", "none");
        $(".fecha").css("display", "none");
        $(".fecharange").css("display", "block");
    };

});
</script>
</html>
